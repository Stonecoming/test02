package com.huawei.cse.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-01-10T12:18:18.829Z")

@RestSchema(schemaId = "projectj76v")
@RequestMapping(path = "/cse", produces = MediaType.APPLICATION_JSON)
public class Projectj76vImpl {

    @Autowired
    private Projectj76vDelegate userProjectj76vDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userProjectj76vDelegate.helloworld(name);
    }

}
